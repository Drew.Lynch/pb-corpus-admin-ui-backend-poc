# To setup install the following packages
- npm install express multer aws-sdk axios

# To run this POC backend
- open bash window in the root of this project
- execute `node server.js`

# AWS Configuration
- Currently the AWS credentials are "hard-coded" in the `server.js` file
  - `AWS_ACCESS_KEY_ID`
  - `AWS_SECRET_ACCESS_KEY`
  - `AWS_SESSION_TOKEN`
- Get new creds before you run (and periodically when they expire)
  - Get these values from the AWS "start" page | Access keys
    - https://powerschool.awsapps.com/start/#/?tab=accounts
    - unified-home-preproduction
    - 246597006913 | unifiedhome-preproduction@powerschool.com
    - PowerUserAccess|Access keys 

- This is POC code, so obviously these should be moved out of the code and placed in environmental variables

# S3 Bucket 
- Bucket name - `dev-test-dlynch-d1bd565e`
- Bucket Access Point alias - `dev-test-dlynch-d1bd-b3hmy4uruiwxyh1czrhmsborifn7euse1b-s3alias`
  - Note: using "Bucket Access Point Name" does not work
- Currently these are hard coded in `server.js` file 

# To test the "hello-world" end point run the following curl command 
```
curl 'http://localhost:3000/hello-world' \
  -X 'GET' \
  -H 'accept: application/json' \
  --compressed
```

# To test File Upload to S3 
- Open the `sandbox-test-upload.html` in a browser
- click CHOOSE FILE button and select a file
- adjust the "tags" 
- click the UPLOAD FILE button

# To test the web crawl
- Open the `sandbox-test-upload.html` in a browser
- set the URL value in the `Start Web Crawl URL` text box
- Click the START WEB CRAWL button 

# If you want to integrate Apify Webhooks then install ngrok 
- Note: Apify Webhooks are used to notify another API when something has finished
- Ngrok provides  web forwarding functionality that can expose your "localhost" to the internet so you can test Apify Webhooks
- Windows - open Command prompt or PowerShell with elevated permissions
- then run the following
- `choco install ngrok`
- Need to signup ngrok.com for a free developer license, this will provide web forwarding to your local machine
  - After signing up, you want to copy the "auth-token" that is automatically created for you once you've signed up
  - Take the auth-token and execute the following in a Command prompt: 
    - `ngrok config add-authtoken your-token-value`
- When your 'node server.js' is running then 
- open command window and execute:
- `ngrok http 3000`
- It will display the URL forwarding (ex:  https://8c97-173-67-147-155.ngrok-free.app -> http://localhost:3000)
- You can also see the URL here https://dashboard.ngrok.com/cloud-edge/endpoints and see the new endpoint (note these are not permanent they are temporary)




