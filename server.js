import express from 'express';
import multer from 'multer';
import AWS from 'aws-sdk';
import axios from 'axios';
import {
    APIFY_HEADER,
    APIFY_GET_HEADER,
    POLL_STATUS_DELAY,
    webCrawlerRunConfig,
    POLL_STATUS_MAX_RETRIES,
    AWS_S3_MAX_FILE_SIZE
} from './constants.js';

// const upload = multer({ dest: 'uploads/' }); // this  uses multer with DiskStorage to store files locally
const upload = multer({ storage: multer.memoryStorage() }); // this uses multer with MemoryStorage to store files in memory

const app = express();

// parse application/json - necessary to parse the body of PUT/POST requests
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // necessary to parse the body of PUT/POST requests and parsing the URL-encoded data with the "qs" library

// AWS configuration - NOTE these are temporary credentials (I'm not sure on the exact time frame) 
// get these values from the AWS "start" page | Access keys
//    https://powerschool.awsapps.com/start/#/?tab=accounts
//    unified-home-preproduction
//    246597006913 | unifiedhome-preproduction@powerschool.com
//    PowerUserAccess|Access keys 
AWS.config.update({
    accessKeyId: 'ASIATS2STWZAZGTTKP6V',
    secretAccessKey: 'g2/Abpz9veW1GizP8HeFTPUhg9T5vvmH0lqZr58i',
    sessionToken: 'IQoJb3JpZ2luX2VjEL7//////////wEaCXVzLWVhc3QtMSJHMEUCIHe+dLEdYPzNVdMbCtEBJGC7IDzTxtYx1o5mQ4KF6h4NAiEAind7oPmjqfv52NaUaUNpno4N+vo3e2e+kR030QDE+8QqngMIRxACGgwyNDY1OTcwMDY5MTMiDF4+AZPvpVV/A7pqWyr7AgAaEeszVSNRtiPOGU1nP3AlenHIZWGk8yQAJH1srffcQaOhMWlNpAJ2vf3xSxE5TBMQ7Sw6Ru/qGp7ZIMFvRcmq+WknISiNaUZxe2XeHRiwaoDRkLxkKacmaxnCzHn1IYvl0FFsuhk2Yks901E+KAsc3OleGeaHHnS4rO1AgPsviDHPcHY6Geau4KdFCIQOvPgDDhjA9v1AfodIGPTKqNYWh7W9aHHRotriDoCd9QWbe7lcGsfJcuIKcNyn8XjfEbtWYfsZUjuFJU8uD0pd9j/yAVyUbEfsohhaE0aT9aAlh038/t0e6VSMmaEvqDvIX0is7ofGXVNPwCsovLXtMn/UkyvjTI7nTnCH1GLMyBEVi/Y59SPOGgt3C3dgQHOrADfhSNbOBvFxiTOFB0/vYrwkUxnjVAJF3FlQhSCiP1+Dp3rYscSKZSv/0/qtGS8FCDubiOqTwX4pB8TcL2Skj+TY9m2SSA/SNB+FZ6SC+AH5CPJLirtFK+9hDSMwxKbnsgY6pgGnHNMXyEgZlAeMZNL4VwqpXVdmvR9A7shkRI6GuqVnZEBPPgweCB31TLU+Ux7jrYLsdSmGhohcvzlc/T8dkmId/Oa9M61xIOcpy+3/xA+AlP3ovEMG18GpBwE0oyvi00/4SQJ+QbD1W1Qis8INQ8u7517b/67PtP2BM2/djQ5dE5b9ExvqIkyiLdE6MqCUGRZabG7Qpxl54Gv3Uzb//zqIcdL2JL2z',
    region: 'us-east-1'
});

const s3 = new AWS.S3();

// S3 Bucket Hard coded values
const accessPointName = 'dev-test-dlynch-d1bd565e-access-point1'; // <-- access point name does not work;
const accessPointAlias = 'dev-test-dlynch-d1bd-b3hmy4uruiwxyh1czrhmsborifn7euse1b-s3alias'; //<-- Access Point Alias works
const bucketName = 'dev-test-dlynch-d1bd565e'; //<-- Bucket Name works


/* ------------------------ API Endpoints ------------------------ */

// GET /hello-world endpoint
app.get('/hello-world', (req, res) => {
    res.send('Hello world');
});

// GET /getFiles endpoint
// http://localhost:3000/getFiles
app.get('/getFiles', (req, res) => {
    console.log(`\n`);
    console.log(`GET /getFiles - here 1`);
    let fileData = [];

    function listAllObjects(token) {
        let params = {
            Bucket: bucketName,
            ContinuationToken: token
        };

        s3.listObjectsV2(params, async (err, data) => {
            if (err) {
                console.log(err, err.stack);
                res.status(500).send(err);
            } else {
                let files = data.Contents;
                try {
                    await Promise.all(files.map(file => {
                        return new Promise((resolve, reject) => {
                            s3.getObjectTagging({ Bucket: bucketName, Key: file.Key }, (err, tagData) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    fileData.push({
                                        FileName: file.Key,
                                        DateStored: file.LastModified,
                                        FileSize: file.Size,
                                        Tags: tagData.TagSet
                                    });
                                    resolve();
                                }
                            });
                        });
                    }));

                    if (data.IsTruncated) {
                        listAllObjects(data.NextContinuationToken);
                    } else {
                        fileData.sort((a, b) => a.FileName.localeCompare(b.FileName));
                        res.status(200).send(fileData.map(JSON.stringify).join('<br/>'));
                    }
                } catch (err) {
                    console.log(err, err.stack);
                    res.status(500).send(err);
                }
            }
        });
    }

    listAllObjects();
});

// POST /upload endpoint
app.post('/admin-ui-file-upload', upload.fields([{ name: 'fileToUpload', maxCount: 1 }, { name: 'tags', maxCount: 1 }]), (req, res) => {
    console.log(`\n`);

    const fileUploaded = req.files['fileToUpload'][0];
    const fileName = fileUploaded.originalname;
    console.log(`POST /admin-ui-file-upload - fileName: ${fileName}`);
    // console.log(`req.file.filename: ${req.file.filename} `); // req.file.filename - only available if using multer with diskStorage
    const fileSize = fileUploaded.buffer.toString().length; // req.file.buffer - only available if using multer with memoryStorage
    // tag restrictions: 
    /*
    The total length of a tag key and value can be no more than 256 Unicode characters.
    The tag key and value are case-sensitive.
    The tag key can be up to 128 Unicode characters.
    The tag value can be up to 256 Unicode characters.
    The tag value cannot contain a comma "," 
    The tag value can contain leters, numbers, spaces, and the following symbols: + - = . _ : / @
    */
    // using "+" = it stripped out the "+" inbetween when I retrieve the tag values of this file
    // const uploadTags = 'audience=students+guardians+teachers&tag=Academic+Compliance+Policy&Active=true';
    // using "@" = works
    //const uploadTags = 'audience=students@guardians@teachers&tag=Academic@Compliance@Policy&Active=true';
    // using "/" = works
    // const uploadTags = 'audience=students/guardians/teachers&tag=Academic/Compliance/Policy&Active=true';
    const uploadTags = req.body.tags;

    const params = {
        Bucket: accessPointAlias, // note this can be the bucket name or an Access Point Alias
        Key: fileUploaded.originalname,
        Body: fileUploaded.buffer,
        Tagging: uploadTags
    };
    s3.upload(params, (err, data) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.status(200).send(`File uploaded successfully to S3 - filename = "${fileName}", fileSize = ${fileSize}, tags=${uploadTags}`);
    });

});

// PUT /Update the Tags
app.put('/updateTags/:key', (req, res) => {
    console.log(`\n`);
    console.log(`PUT /updateTags/${req.params.key}`);

    // Note: Key is the file name
    const key = req.params.key;
    const newTags = req.body.newTags; // newTags is an array of objects. Ex: {"newTags": [{"Key": "audience","Value": "students/guardians/teachers"},{"Key": "tag","Value": "Academic/Compliance/Policy"},{"Key": "Active","Value": "true"}]}

    const params = {
        Bucket: bucketName,
        Key: key,
        Tagging: {
            TagSet: newTags
        }
    };

    s3.putObjectTagging(params, (err, data) => {
        if (err) {
            console.log(err, err.stack);
            res.status(500).send(err);
        } else {
            res.status(200).send(`Tags updated successfully for file "${key}"`);
        }
    });
});

// POST / Start the webcrawl process
app.post('/startWebCrawl', async (req, res) => {
    console.log(`\n`);
    console.log(`POST /startWebCrawl - req.body:`);
    console.log(req.body);
    const startWebCrawlUrl = req.body.startWebCrawlURL;
    console.log(`Received URL: ${startWebCrawlUrl}`);

    try {
        const beginWebCrawlResult = await beginWebCrawl(startWebCrawlUrl);
        // Immediately respond to the client with the process started message AND the runDataId
        res.status(200).send(`Process started - runDataId: ${beginWebCrawlResult.data.id} - crawling URL: ${startWebCrawlUrl}`);
        monitorAndProcessWebCrawl(startWebCrawlUrl, beginWebCrawlResult.data.id);
    } catch (error) {
        res.status(500).send({ error: 'An error occurred while ttempting to webcrawl.' });
    }
});

// GET /webcrawl Log
app.get('/webCrawlLog/:runDataId', async (req, res) => {
    console.log(`\n`);
    const runDataId = req.params.runDataId;
    console.log(`GET /webCrawlLog - runDataId: ${runDataId}`);

    try {
        const webCrawlLogResults = await getWebCrawlLog(runDataId);
        res.status(200).send(webCrawlLogResults);
    } catch (error) {
        res.status(500).send({ error: 'An error occurred while fetching the web crawl log.' });
    }
});

// Webhook test for Apify using "ngrok" to expose localhost to the internet
// see readme.md for more information on "ngrok"
app.post('/webCrawlResult', (req, res) => {
    const data = req.body.data;

    console.log(`\n`);
    console.log(`POST /webCrawlResult - req.body:`);
    console.log(req.body);
    /* this is an example return from req.body 
    {
        "userId":"M7W2fzyykxdVmzhx9",
        "createdAt":"2024-05-29T22:50:03.545Z",
        "eventType":"ACTOR.RUN.SUCCEEDED",
        "eventData":{
            "actorId":"aYG0l9s7dbB7j3gbS",
            "actorRunId":"oRPMudtQHv1d55LvM"
        },
        "resource":{
            "id":"oRPMudtQHv1d55LvM",
            "actId":"aYG0l9s7dbB7j3gbS",
            "userId":"M7W2fzyykxdVmzhx9",
            "startedAt":"2024-05-29T22:46:53.184Z",
            "finishedAt":"2024-05-29T22:49:57.413Z",
            "status":"SUCCEEDED",
            "statusMessage":"Finished! Total 5 requests: 5 succeeded, 0 failed.",
            "isStatusMessageTerminal":true,
            "meta":{
                "origin":"WEB",
                "userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36"
            },
            "stats":{
                "inputBodyLen":1106,
                "rebootCount":0,
                "restartCount":0,
                "durationMillis":184100,
                "resurrectCount":0,
                "runTimeSecs":184.1,
                "metamorph":0,
                "computeUnits":0.20455555555555555,
                "memAvgBytes":760675629.8235221,
                "memMaxBytes":1149689856,
                "memCurrentBytes":63139840,
                "cpuAvgUsage":76.87793065397354,
                "cpuMaxUsage":203.97566694987256,
                "cpuCurrentUsage":7.569804662885948,
                "netRxBytes":27646365,
                "netTxBytes":2607287
            },
            "options":{
                "build":"version-0",
                "timeoutSecs":360000,
                "memoryMbytes":4096,
                "diskMbytes":8192
            },
            "buildId":"j84aedrLqMwGmoP95",
            "exitCode":0,
            "defaultKeyValueStoreId":"vKFUK5hb7oUGtuFx7",
            "defaultDatasetId":"Q3LYkXF5ZFq1xC4CG",
            "defaultRequestQueueId":"lAYRpahP9OdR9R5eX",
            "buildNumber":"0.3.35",
            "containerUrl":"https://g2a0tuo2udvw.runs.apify.net",
            "usage":{
                "ACTOR_COMPUTE_UNITS":0.20455555555555555,
                "DATASET_READS":0,
                "DATASET_WRITES":4,
                "KEY_VALUE_STORE_READS":1,
                "KEY_VALUE_STORE_WRITES":28,
                "KEY_VALUE_STORE_LISTS":0,
                "REQUEST_QUEUE_READS":16,
                "REQUEST_QUEUE_WRITES":53,
                "DATA_TRANSFER_INTERNAL_GBYTES":0.02153773419559002,
                "DATA_TRANSFER_EXTERNAL_GBYTES":0.002144784666597843,
                "PROXY_RESIDENTIAL_TRANSFER_GBYTES":0,
                "PROXY_SERPS":0
            },
            "usageTotalUsd":0.0858770658653213,
            "usageUsd":{
                "ACTOR_COMPUTE_UNITS":0.08182222222222223,
                "DATASET_READS":0,
                "DATASET_WRITES":0.00002,
                "KEY_VALUE_STORE_READS":0.000005,
                "KEY_VALUE_STORE_WRITES":0.0014,
                "KEY_VALUE_STORE_LISTS":0,
                "REQUEST_QUEUE_READS":0.000064,
                "REQUEST_QUEUE_WRITES":0.0010600000000000002,
                "DATA_TRANSFER_INTERNAL_GBYTES":0.001076886709779501,
                "DATA_TRANSFER_EXTERNAL_GBYTES":0.00042895693331956863,
                "PROXY_RESIDENTIAL_TRANSFER_GBYTES":0,
                "PROXY_SERPS":0
            }
        }
    }
    */
    res.status(200).send("Webhook received successfully");
});


/* ------------------------ Helper Functions ------------------------ */

async function getWebCrawlLog(runDataId) {
    try {
        const response = await axios.get(`https://api.apify.com/v2/logs/${runDataId}`, APIFY_HEADER);
        return response.data;
    } catch (error) {
        console.error(error);
        throw error;
    }
}


async function beginWebCrawl(startWebCrawlUrl) {
    try {
        // Set the start URL for the webcrawler
        webCrawlerRunConfig.startUrls = [{ url: startWebCrawlUrl }];

        // Call the Apify endpoint to run the webcrawler to start the long running process
        const response = await axios.post('https://api.apify.com/v2/acts/apify~website-content-crawler/runs',
            webCrawlerRunConfig,
            APIFY_HEADER);
        console.log(`beginWebCrawl - response.data:`);
        console.log(response.data);
        return response.data;

    } catch (error) {
        console.error(error);
    }
}

async function monitorAndProcessWebCrawl(startWebCrawlUrl, runDataId) {
    try {
        const pollWebCrawlStatusResult = await pollWebCrawlStatus(runDataId);

        console.log(`\n`);
        console.log(`monitorAndProcessWebCrawl - pollWebCrawlStatusResult:`);
        console.log(pollWebCrawlStatusResult);


        if (pollWebCrawlStatusResult.data.status === "SUCCEEDED") {
            console.log(`Webcrawl succeeded`);
            fetchAndUpload(startWebCrawlUrl, pollWebCrawlStatusResult.data.id, pollWebCrawlStatusResult.data.defaultDatasetId, pollWebCrawlStatusResult.data.defaultKeyValueStoreId);
        } else {
            console.log(`Webcrawl FAILED`);
        }
    } catch (error) {
        console.error(error);
    }
}

// This function will poll the Apify API every 10 seconds to check the status of the webcrawl
async function pollWebCrawlStatus(runDataId) {
    let status = "";
    let response;
    let retries = 1;
    while (status !== "SUCCEEDED" && status !== "FAILED" && retries <= POLL_STATUS_MAX_RETRIES) {
        response = await checkWebCrawlStatus(runDataId);
        status = response.data.status;
        console.log(`pollWebCrawlStatus every 10 seconds, runDataId: ${runDataId} Status: ${status}`);
        if (status !== "SUCCEEDED" && status !== "FAILED") {
            await new Promise(resolve => setTimeout(resolve, POLL_STATUS_DELAY));
            retries++;
        }
    }
    if (retries > POLL_STATUS_MAX_RETRIES) {    // if we've reached the max retries, return the last response   
        response.data.status = "INTERNAL-TIMEOUT";
        console.log(`pollWebCrawlStatus - max retries reached`);
    }
    return response;
}

async function checkWebCrawlStatus(runDataId) {
    try {
        // Call the Apify endpoint to run the webcrawler to start the long running process
        const response = await axios.get(`https://api.apify.com/v2/actor-runs/${runDataId}`, APIFY_HEADER);

        console.log(response.data); // looking for data.status to be "SUCCEEDED" or "FAILED" ("READY and "RUNNING" are interim statuses)
        return response.data

    } catch (error) {
        console.error(error);
    }
}


// This function will fetch the webcrawl data from Apify and upload it to S3
async function fetchAndUpload(webCrawlUrl, runDataId, defaultDatasetId, defaultKeyValueStoreId) {
    try {
        const fileName = `Web-Crawl/${sanitizeUrl(webCrawlUrl)}`;
        console.log(`fetchAndUpload for runDataId: S3 file-name will be: ${fileName}, runDataId: ${runDataId}, defaultDatasetId: ${defaultDatasetId}, defaultKeyValueStoreId: ${defaultKeyValueStoreId} `);
        const apifyFileFormat = 'format=csv&fields=url%2Cmarkdown%2Ctext';
        const response = await axios.get(`https://api.apify.com/v2/datasets/${defaultDatasetId}/items?${apifyFileFormat}`, {
            APIFY_GET_HEADER,
            responseType: 'arraybuffer' // to handle binary data
        });
        // Saving the following tags:
        // WebCrawl=true
        // rundataid=runDataId <-- use this this value to obtain the run LOG from the Apify website
        // defaultDatasetId=defaultDatasetId <-- use this value to obtain the dataset from the Apify website
        // defaultKeyValueStoreId=defaultKeyValueStoreId <-- use this value to obtain the INPUT information about the run of this webcrawl
        const tags = 'WebCrawl=true&runDataId=' + runDataId + '&defaultDatasetId=' + defaultDatasetId + '&defaultKeyValueStoreId=' + defaultKeyValueStoreId;
        const params = {
            Bucket: accessPointAlias,
            Key: fileName,
            Body: response.data,
            Tagging: tags,
        };

        s3.upload(params, (err, data) => {
            if (err) {
                console.log('Error uploading data: ', err);
            } else {
                console.log('Successfully uploaded data');
            }
        });
    } catch (error) {
        console.log('Error fetching data: ', error);
    }
}

// This function will sanitize the URL to be used as the S3 file name
function sanitizeUrl(url) {
    // Remove the protocol
    let sanitizedUrl = url.replace(/(^\w+:|^)\/\//, '');

    // Replace all non-alphanumeric characters with a hyphen
    sanitizedUrl = sanitizedUrl.replace(/[^a-z0-9]/gi, '-');

    // Remove any trailing hyphens
    sanitizedUrl = sanitizedUrl.replace(/-+$/, '');

    // max length of S3 file names is 1024 characters
    const fileExtension = '.csv';
    const fileNameMaxLength = AWS_S3_MAX_FILE_SIZE - fileExtension.length;
    if (sanitizedUrl.length > fileNameMaxLength) {
        sanitizedUrl = sanitizedUrl.substring(0, fileNameMaxLength);
    }
    // Append .csv to the end of the file name
    // ex: webcrawl URL = https://www.powerschool.com/powerschool-ai/
    // santiized URL = Web-Crawl/www-powerschool-com-powerschool-ai
    sanitizedUrl += fileExtension;

    return sanitizedUrl;
}

app.listen(3000, () => console.log('Server started on port 3000'));
