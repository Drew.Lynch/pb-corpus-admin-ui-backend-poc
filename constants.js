export const webCrawlerRunConfig = {
    "aggressivePrune": false,
    "clickElementsCssSelector": "[aria-expanded=\"false\"]",
    "clientSideMinChangePercentage": 15,
    "crawlerType": "playwright:firefox",
    "debugLog": false,
    "debugMode": false,
    "ignoreCanonicalUrl": false,
    "proxyConfiguration": {
        "useApifyProxy": true
    },
    "readableTextCharThreshold": 100,
    "removeCookieWarnings": true,
    "removeElementsCssSelector": "nav, footer, script, style, noscript, svg,\n[role=\"alert\"],\n[role=\"banner\"],\n[role=\"dialog\"],\n[role=\"alertdialog\"],\n[role=\"region\"][aria-label*=\"skip\" i],\n[aria-modal=\"true\"]",
    "renderingTypeDetectionPercentage": 10,
    "saveFiles": true,
    "saveHtml": false,
    "saveMarkdown": true,
    "saveScreenshots": false,
    "startUrls": [
        {
            "url": "https://www.powerschool.com/powerschool-ai/"
        }
    ],
    "useSitemaps": false,
    "includeUrlGlobs": [],
    "excludeUrlGlobs": [],
    "maxCrawlDepth": 20,
    "maxCrawlPages": 9999999,
    "initialConcurrency": 0,
    "maxConcurrency": 200,
    "initialCookies": [],
    "maxSessionRotations": 10,
    "maxRequestRetries": 5,
    "requestTimeoutSecs": 60,
    "minFileDownloadSpeedKBps": 128,
    "dynamicContentWaitSecs": 10,
    "maxScrollHeightPixels": 5000,
    "htmlTransformer": "readableText",
    "maxResults": 9999999
}
// export const APIFY_API_KEY = "apify_api_Rnh7lEMjVBMmTBadHZnScRLkSccC9Z2zLVSl"; //<-- DLynch API Key
export const APIFY_API_KEY = "apify_api_EzSPbtOc3iiZNEEBVC45o8dK4odzGi0EzOl7"; //<-- Organization's API KEY
export const APIFY_HEADER = {
    headers: {
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.9',
        'Authorization': `Bearer ${APIFY_API_KEY}`,
    }
}
export const APIFY_GET_HEADER = {
    headers: {
        'authorization': `Bearer ${APIFY_API_KEY}`,
        'accept': 'application/json'
    }    
}
export const POLL_STATUS_DELAY = 10000;
export const POLL_STATUS_MAX_RETRIES = 120; // 120 iterations at 10 seconds each = 20 minutes
export const AWS_S3_MAX_FILE_SIZE = 1024;